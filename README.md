# Proposal for safe cross-shard communication for Harmony One

This document proposes a design of an asynchronous message passing
interface for smart contracts running on OpenHarmony, based on
MuKn's earlier [proposal](https://mukn.io/2021/09/09/safe-cross-chain-contract-interactions/).

# Overview

For safety purposes, we propose a design based on explicit message passing
rather than on cross-shard contract calls.
Smart contract developers can use Solidity (or the EVM-targeting language of their choice)
to call a new special "precompiled" contract, the "(message) router",
that can send and receive messages across shards.
To avoid spam attacks, sending message must both pay for gas on the
sending shard, and reserve tokens to pay for gas on the receiving shard.
To avoid messages being stuck with too little gas (possibly due to
malicious attack), additional tokens can be contributed to this reserve
until the message is received.

# Router Address

The router contract would have a well-known address distinct from any
possible "real" address. We propose choosing a small integer address
as for other "precompiled contracts." at time of writing the next
available number for precompiled contracts is 252.

# Spam Mitigation

OpenHarmony's current design for cross-shard transactions suffers from
a possible spam vector: since gas costs for the transactions are paid on
the sending shard, spammers could exploit lower gas costs on other
shards to overwhelm a single shard with messages, without paying the
higher gas fees on the target shard.

We propose handling this problem by requiring the sender to provide
funds for gas in the form of the chain's native token, instead of
paying directly in gas. These funds will be earmarked for the
purchase gas on the target shard, as opposed to separate funds
being transmitted to the recipient on the target shard.

When the miners for a receiving shard synchronize past a sending shard
block that contains a message, they *may* include that message in their
receiving shard block. Including that message will consume an
appropriate amount of gas toward the block gas limit, in exchange for
which the miner will receive the earmarked tokens as reward.
Miners may also ignore the message if it does not include sufficient earmarked tokens,
at which point the message can be included by miners of future blocks.

If miners for the receiving shard consistently ignore a message,
the users who care to have it be received can supply extra tokens to
deliver the message, by calling a method on the router contract, passing
it the *message address* and supplying the necessary additional funds.
This method call must happen on the sending shard.

The *message address* is computed based on the hash of a serialization
of the message metadata itself including a hash of the actual data. The
message address is not a "real" account address which can be called or
to which funds can be sent, but the format is the same, and addresses
are chosen according to an algorithm that avoids any collision with
previous kinds of addresses, so it is possible to make them "real"
addresses in the future should that be desired.

# Router Methods

This section concretely documents a possible set of methods for the
router contract.

## Sending Messages

Sending messages uses the send method:

```
function send(address to_,
              shardId toShard,
              bytes payload,
              uint256 gasBudget,
              uint256 gasPrice,
              uint256 gasLimit,
              address gasLeftoverTo) returns (address msgAddr)
```

`send` queues an asynchronous message to be sent to a contract.
The recipient contract may or may not be on a different shard from
the sending contract.

The `gasBudget` parameter must be less than the value of native tokens
sent with the call (in Solidity, using the usual `.value()` syntax).
The sent message will include the payload as data
and a transferred value of `<called value> - gasBudget`. When the
message arrives at the target shard, up to `gasBudget` tokens will be
used to purchase gas with which to deliver the incoming message to
the target. `gasPrice` and `gasLimit` specify the gas price &
limit for the transaction in which the message is accepted.
Any left over gas will be sent to `gasLeftoverTo` on the receiving
shard. `gasBudget` must be at least `gasPrice * gasLimit`. Note that
if the receiving transaction aborts, any left over gas budget will
still be delivered to `gasLeftoverTo`.

Returns a unique address for the message, which can be used by the
accepting contract, or in the event that further funds need to be
supplied (see below).

## Gas Costs

We propose that, on the sending shard, sending a message should cost:

- 20,000 gas per word in the message,
- plus the baseline cost of the `CALL` instruction,
- plus `10 * 20,000 = 200,000 gas`

In addition, on the receiving shard the sender must pay for:

- The gas that is consumed by the receiving contract in response
  to the method call,
- plus the cost of a `CALL` instruction necessary to transfer any
  leftover `gasBudget` to the address specified in `gasLeftoverTo`,
- plus 20,000 gas.

Rationale:

- 20,000 gas is the cost of storing one word of data via
  [SSTORE](https://github.com/djrtwo/evm-opcode-gas-costs/blob/master/opcode-gas-costs_EIP-150_revision-1e18248_2017-04-12.csv#L50),
  and we believe the computational and storage requirements are
  similar, since:
  - On the sending shard, the data must be stored indefinitely
    in case retransmission is later required. 10 extra words
    of metadata also need to be stored, in addition to the
    message payload.
  - On the receiving shard, a small amount of space must be
    permanently dedicated to recording the message's delivery.
- Other than additional permanent storage, once the message
  arrives at the receiving shard, computational and storage
  requirements should be comparable to a normal transaction.

The gas to be paid on the sending shard should be included in the
gas for the CALL, per usual, and the gas for the receiving shard
will be purchased using `gasBudget` when the message is received.

### Underfunded Messages

If the `gasBudget` or `gasLimit` sent with a message is insufficient to
cover gas costs on the receiving shard, or `gasPrice` is too low for the
message to be prioritized, then the receiving shard will ignore the
message. In this case, parties wanting to see the message delivered
can supply additional funds by invoking the `retrySend` method:

function retrySend(address msgAddr,
                   uint256 gasLimit,
                   uint256 gasPrice)

Where `msgAddr` should be the address originally returned by `send()`.
The funds themselves should be attached to the call, in Solidity again
using the usual `.value()` syntax. The funds will be added to the
`gasBudget` associated with the message, and the sending shard will
retry sending the message. The `gasLimit` and `gasPrice` fields update
the values originally set with `send()`. `gasPrice` and `gasLimit` must
not be lower than the existing values, and the new `gasBudget` must be
at least the new value of `gasLimit * gasPrice`.

If the receiving shard accepts the original message and then sees a
re-transmission, it will credit additional funds to the value of
`gasLeftoverTo` in the original call, but it will not invoke the
receiving contract a second time.

The gas cost for invoking `retrySend` itself is the baseline cost of
the `CALL` instruction, plus `3 * 20,000 = 60,000` gas, since this
updates up to 3 words of storage.

# Accepting messages

When a receiving shard accepts a message, it calls the following
method on the receiving contract, in its own transaction:

```
recvCrossShardMessage(address fromAddr,
                      shardId fromShard,
                      bytes payload)
```

`fromAddr` is the address of the sender on the sending shard. If the
contract executes the `CALLER` instruction, it will return the router's
address.

The method call also includes any tokens transferred by the message,
included in the call in the usual way.

# Message Address Format

We propose choosing computing message addresses as:

```
keccack256(
    0xff ++
    fromAddr ++
    toAddr ++
    fromShard ++
    toShard ++
    keccack256(payload) ++
    value ++
    nonce)[12:]
```

Where:

- `fromAddr` is the address of the sending contract.
- `toAddr` is the address of the receiving contract.
- `fromShard` is the shard id of the sending contract
- `toShard` is the shard id of the receiving contract.
- `payload` is the data of the message itself
- `value` is the value in native tokens transferred by the call,
  not counting the gas budget.
- `nonce` is a 128-bit nonce. Nonces are chosen sequentially per sending
  contract, so the first message a contract sends over its lifetime
  uses nonce 0, the second uses 1, etc. This counter is separate
  from the nonce used by the `CREATE` instruction.

Values with sizes greater than one byte are encoded in little-endian
format.

Note that `(fromAddr, nonce)` would be sufficient to uniquely identify
a message, but incorporating the rest of the metadata allows the
message address to be used to validate the message itself.

This address is guaranteed to be unique:

- The `0xff` avoids colliding with `CREATE` addresses, by the same
  reasoning as described in EIP-1014, for `CREATE2`.
- The length of the data hashed is different from other address types,
  so it will not collide with them either.

# Protocol Changes

This section describes data structure & protocol level changes needed to
support the above interfaces.

## Storage

There are two new data structures that need to be tracked in order to
support cross-shard messages:

1. A "messages received" map, which keeps track of which messages
   have already been received & delivered, and what their gas budget
   was. This is needed to handle duplicate deliveries correctly.
2. A "messages outbox," which contains data for messages which have
   been sent but not necessarily received, in case they must be
   re-transmitted.

We propose storing each of these in the contract storage for the router
contract.

### Messages Received

The messages received map stores the `gasBudget` associated with
already-delivered messages, keyed by the message address. If the
recorded `gasBudget` is zero, this means that that message has
not been delivered before (It always is safe to use zero as a
sentinel like this, since a message with no gas will by definition
not have enough funds to be accepted).

Concretely, we load & store the gas budget for a particular message
address by:

1. Right-padding the message address with zeros
2. Executing an `SLOAD` or `SSTORE` instruction, with the padded
   address as the key.

Use of the messages received map is described in more detail in the
section "Receiver  Changes," below.

### Messages Outbox

The entries in the messages outbox are larger than 1 word. We propose
storing the first word of the message metadata at:

```
message address ++ 0x01 ++ <11 zero bytes>
```

...where the 0x01 makes sure it does not collide with the messages
received map. The remaining bytes of the metadata are stored
afterwards. In order, we store:

- The sending address
- The receiving address
- The receiving shard
- Nonce used to generate the message address
- Number of native tokens transferred
- `gasBudget`
- `gasPrice`
- `gasLimit`
- `gasLeftoverTo`
- The length of the payload, in bytes.
- The `keccack256` hash of the message payload

The receiving shard and the nonce can be packed into a single word (16
byte nonce + 4 byte shard id). Each other field takes one word of
storage, for a total of 10 words.

The first word of the payload itself is stored at the address matching
the payload's hash, and the remainder of the payload is stored in
consecutive words. We rely on the collision resistance of the hash
function to avoid duplicate addresses.

If the length of the payload is not a whole number of words, the last
word is padded with zeros.

## CXReceipt changes

The `CXReceipt` data structure defined in `core/types/cx_receipt.go`
would gain an additional (optional) `MessageSend` field,
If this field is present, it indicate that the receipt is for a
message send, rather than just a balance transfer:

```
 type CXReceipt struct {
     TxHash    common.Hash
     From      common.Address
     To        *common.Address
     ShardID   uint32
     ToShardID uint32
     Amount    *big.Int
+
+    // If non-nil, this is an asynchronous message send, rather than just
+    // a balance transfer:
+    MessageSend *CXMessageInfo
 }
```

This field contains additional information about the message:

```go
type CXMessageInfo struct {
    // Each of these corresponds to the named parameter in the Router's
    // send() method. Must satisfy:
    //
    // - GasBudget <= CXReceipt.Amount.
    // - GasBudget >= GasLimit * GasPrice
    GasBudget, GasLimit, GasPrice *big.Int
    GasLeftoverTo common.Address

    // A nonce, unique per message sender pair, used when computing the
    // message address.
    Nonce [16]byte

    // The address of the message itself. This could in principle be
    // left out and computed from the other information in CXReceipt.
    Address common.Addresss

    // Actual data in the message
    Payload []byte
}
```

## Sender Changes

When a transaction sends messages to other shards, using the magic
contract interface described above, a receipt is generated for each
message, and these are transmitted to the sending shard in the same
manner as existing cross-shard transactions.

The sending shard also records the message by adding it to the
messages outbox stored in the router contract's storage, as
described above.

If a transaction sends funds to an address corresponding to a message,
a *new* receipt is generated for the same message address, and the
message is re-transmitted. It is the receiving shard's responsibility
to ensure that the message is not delivered more than once.

## Receiver Changes

When the receiving shard sees an incoming receipt, it should:

- Look the message address up in the messages received map, as
  described above.
- If the map contains the value zero, use the information in the
  receipt to purchase gas needed to validate and deliver the message
  (by calling the target's `recvCrossShardMessage()` method).
  - If the funds are insufficient, the shard should ignore the message;
    parties interested in its delivery will have to supply additional
    funds on the sending shard.
  - Assuming the funds are sufficient, store the `gasBudget` in the
    messages received map, and credit any leftover funds to `gasLeftoverTo`.
    Note: do this even if the call's transaction aborts.
- If the stored value is *not* zero, credit `newGasBudget - oldGasBudget`
  to `gasLeftoverTo`, and record the new gas budget in the storage, but
  do not perform delivery again.

# Example Usage: User Defined Tokens

This section describes a hypothetical interface for user defined tokens
à la ERC20. ERC20 cannot be used *directly* for multi-shard user defined
tokens because it is based around solidity methods, which cannot in
general be called cross-shard. Indeed, preventing this is the point of
defining the special `recvCrossShardMessage` method; existing ERC20
implementations may not be safe to use in a multi-shard environment.

For the sake of illustration, we keep the design as close to ERC20 as
possible; existing contracts should require only minor code changes
to support the new standard.

## Encoding Method Calls

We cannot directly use the solidity ABI for method calls, since the
message must be sent as a call to the special `recvCrossShardMessage`
method. Instead, we require a convention for encoding higher-level
"method calls" into the payloads of cross-shard messages. The
`recvCrossShardMessage` method would then decode the payload
according to our convention, call the appropriate method, encode
the return value and send it back to the original sender.

Since cross shard messages are one-way, we need a convention for
correlating requests/"method calls" with responses/returns. We suggest
including a request ID in the payload, along with the method name and
parameters, as is done by many RPC protocols. The same request ID will
be included in the return message.

In addition, since sending the return message requires additional
payment beyond the original message, we suggest a convention where
the request message includes tokens needed to pay for the return
message; when the receiving contract sends its return, it will set
`gasBudget` equal to the value indicated in the request message.
The recipient will treat `gasBudget` native tokens as being reserved
for the return, and any additional native tokens as part of the message
"proper."

To summarize, a request consists of:

- A request ID
- Encoded method name and parameters
- Information to be used to send the return message: The values of
  `gasBudget`, `gasPrice`, `gasLimit`, and `gasLeftoverTo` that should
   be passed to the router's `send` method. `gasBudget` must be less than
   or equal to the tokens included in the request, and the other
   parameters must satisfy the constraints outlined in the description
   of `send`.

Some suitable binary serialization would have to be chosen; an obvious
choice would be to use the same encoding used for methods in the solidity
ABI.

Additional tooling could be developed to automatically generate the
extra encoding & decoding boilerplate needed to implement this
convention, but note that contracts will still have to deal with the
fact that request send, request delivery, and return delivery all happen
in separate transactions.

## Interface Changes

Beyond just finding a way to encode method calls in cross-shard
messages, the ERC20 API must be adjusted to deal with shard identifiers:
since addresses are not unique per-shard, accounts must now be
identified by an (address, shard id) pair. Methods which take an address
to identify an account now need extra parameters for the corresponding
shard identifiers.

The existing ERC20 API consists of the following methods:

```
function name() public view returns (string)
function symbol() public view returns (string)
function decimals() public view returns (uint8)
function totalSupply() public view returns (uint256)
function balanceOf(address _owner) public view returns (uint256 balance)
function transfer(address _to, uint256 _value) public returns (bool success)
function transferFrom(address _from, address _to, uint256 _value) public returns (bool success)
function approve(address _spender, uint256 _value) public returns (bool success)
function allowance(address _owner, address _spender) public view returns (uint256 remaining)
```

Adapting this for a cross-shard environment, we end up with:

```
function name() public view returns (string)
function symbol() public view returns (string)
function decimals() public view returns (uint8)
function totalSupply() public view returns (uint256)
function balanceOf(address _ownerAddr, shardId _ownerShard) public view returns (uint256 balance)
function transfer(address _toAddr, shardId _toShard,
                  uint256 _value) public returns (bool success)
function transferFrom(address _fromAddr, shardId _fromShard,
                      address _toAddr, shardId _toShard,
                      uint256 _value) public returns (bool success)
function approve(address _spenderAddr, shardId _spenderShard,
                 uint256 _value) public returns (bool success)
function allowance(address _ownerAddr, shardId _ownerShard,
                   address _spenderAddr, shardId _spenderShard) public view returns (uint256 remaining)
```
